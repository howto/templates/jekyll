# Jekyll Pages Template

This is a template that uses Jekyll to create a static web page that is published at git-pages.thm.de.

This template is based on the Jekyll template from GitLab and is adapted for use at git.thm.de. The original template from GitLab can be found here: [https://gitlab.com/pages/jekyll](https://gitlab.com/pages/jekyll).

**Note:** the subpath in the jekyll configuration (File: `_config.yml`) must match the URL slug of your project!
